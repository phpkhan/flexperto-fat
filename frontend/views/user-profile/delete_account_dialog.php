<!-- set up the modal to start hidden and fade in and out -->
<div id="cancelDialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- dialog body -->
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                Are you sure you want to cancel your account?!
            </div>
            <!-- dialog buttons -->
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-success">Cancel</button>
                <button onclick="deleteUser()" data-username="<?= $username ?>" type="button" class="remove-account btn btn-danger">Yes</button>
            </div>
        </div>
    </div>
</div>
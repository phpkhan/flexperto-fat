<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
$this->title = "$model->username's profile";
$this->params['breadcrumbs'][] = "profile-edit";
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1" >
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $model->username; ?>'s profile</h3>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'update-form',
                        'options' => [
                            'enctype' => 'multipart/form-data',
                        ],
                    ]);
                    ?>
                    <div class="row">
                        <div class="img-container col-md-4 col-lg-4 " align="center">
                            <?php $imgName = isset($avatar) ? $avatar : Url::to("@web/images/avatar/avatar.png"); ?>
                            <img alt="User Pic" src="<?php echo "{$imgName}"; ?>" class="img-thumbnail img-responsive">
                            <?=
                            $form->field($model, 'image')->fileInput()
                            ?>
                        </div>
                        <div class=" col-md-8 col-lg-8 ">
                            <table class="table table-user-information">
                                <tbody>
                                <tr>
                                    <td><?= $form->field($model, 'username')->textInput(['placeholder' => $model->username, 'value' => '']) ?></td>
                                </tr>
                                <tr>
                                    <td><?= $form->field($model, 'mobile_number')->textInput(['placeholder' => $model->mobile_number, 'value' => '']) ?></td>
                                </tr>
                                <tr>
                                    <td><?= $form->field($model, 'password')->passwordInput(['placeholder' => '((unchanged))']) ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>


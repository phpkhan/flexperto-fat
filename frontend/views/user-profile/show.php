<?php
/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = "$username's profile";
$this->params['breadcrumbs'][] = "profile";
?>
<div class="container">
    <div class="row">
        <div
            class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <span class="pull-right">
                            <a id='edit-btn' href="<?php echo \yii\helpers\Url::to('/index.php?r=user-profile%2Fedit')?>" type="button" class="btn btn-xs btn-warning action-btns"><i class="glyphicon glyphicon-edit"></i> edit</a>
                            <a id='edit-btn' onclick="deleteUserDialog()" type="button" class="btn btn-xs btn-warning action-btns"><i class="glyphicon glyphicon-edit"></i> delete</a>
                    </span>
                    <h3 class="panel-title"><?php echo $username; ?>'s profile</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4 col-lg-4 " align="center">
                            <?php $imgName = isset($avatar) ? $avatar : Url::to("@web/images/avatar/avatar.png"); ?>
                            <img alt="User Pic" src="<?php echo "{$imgName}"; ?>" class="img-thumbnail img-responsive">
                        </div>
                        <div class=" col-md-8 col-lg-8 ">
                            <table class="table table-hover table-user-profile">
                                <tbody>
                                <tr>
                                    <td>Username:</td>
                                    <td id="username"><?php echo $username; ?></td>
                                </tr>
                                <tr>
                                    <td>Email:</td>
                                    <td id="email"><?php echo isset($email) ? $email : '-'; ?></td>
                                </tr>
                                <tr>
                                    <td>Mobile Number:</td>
                                    <td id="phone"><?php echo isset($mobile_number) ? $mobile_number : '-'; ?></td>
                                </tr>
                                <tr>
                                    <td>Last Updated:</td>
                                    <td id="updated-at"><?php echo isset($updated_at) ? gmdate("Y-m-d H:i:s", $updated_at) : '-'; ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <?= $this->render( 'delete_account_dialog', ['username'=> $username] ); ?>
    </div>
</div>
<?php $this->registerJsFile(Url::to("@web/js/delete-account.js")) ?>


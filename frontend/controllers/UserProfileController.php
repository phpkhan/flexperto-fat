<?php
namespace frontend\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

use common\models\User;
use common\models\UpdateForm;

/**
 * Site controller
 */
class UserProfileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['show', 'edit', 'delete'],
                'rules' => [
                    [
                        'actions' => ['show'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidParamException
     */
    public function actionShow()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Url::to('login'));
        }

        $data = [
            'username' => Yii::$app->user->identity->username,
            'email' => Yii::$app->user->identity->email,
            'mobile_number' => Yii::$app->user->identity->mobile_number,
            'avatar' => Yii::$app->user->identity->avatar,
            'updated_at' => Yii::$app->user->identity->updated_at
        ];

        return $this->render('show', $data);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\Exception
     */
    public function actionEdit()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Url::to('login'));
        }

        $model = new UpdateForm(Yii::$app->user->identity);
        $data = [
            'avatar' => Yii::$app->user->identity->avatar,
            'model' => $model,
        ];

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');

            if ($model->image) {
                $model->upload();
            }

            $model->update();

            return $this->redirect('/user-profile/show');
        } else {
            return $this->render('edit', $data);
        }
    }

    /**
     * @return array
     * @throws \yii\web\NotAcceptableHttpException
     */
    public function actionDelete()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Url::to('login'));
        }

        if (Yii::$app->request->isAjax) {
            $username = Yii::$app->request->post()['username'];

            if ($username === Yii::$app->user->identity->username) {
                $user = Yii::$app->user->identity;
                $user->status = User::STATUS_DELETED;

                if ($user->save()) {
                    Yii::$app->user->logout();
                    $response = ['status' => 'success'];
                } else {
                    $response = ['status' => 'error', 'message' => 'failed to delete account.'];
                }
            } else {
                $response = ['status' => 'error', 'message' => 'invalid request'];
            }

            Yii::$app->response->format = Yii\web\Response::FORMAT_JSON;
            return $response;
        } else {
            throw new Yii\web\NotAcceptableHttpException('Direct access to this action is not allowed.');
        }
    }
}

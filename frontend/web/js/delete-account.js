var deleteUserDialog = function() {
    var cancelDialog = $('#cancelDialog');
    cancelDialog.modal('show');
};

var deleteUser = function() {
    var confirmBtn = $('.remove-account')[0];
    var username = $(confirmBtn).data('username');

    $.ajax({
        url: '/index.php?r=user-profile%2Fdelete',
        type: 'post',
        data: {
            username: username,
            _csrf : '<?=Yii::$app->request->getCsrfToken()?>'
        },
        success: function () {
            window.location.href = '/';
        }
    });
};
<?php

use yii\db\Migration;

/**
 * Handles adding mobile_number to table `user`.
 */
class m160702_083900_add_mobile_number_to_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'mobile_number', $this->string() . ' AFTER email');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'mobile_number');
    }
}
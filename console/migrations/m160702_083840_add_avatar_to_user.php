<?php

use yii\db\Migration;

/**
 * Handles adding avatar to table `user`.
 */
class m160702_083840_add_avatar_to_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'avatar', $this->string(). ' AFTER email');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'avatar');
    }
}

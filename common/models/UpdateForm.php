<?php
namespace common\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\BaseYii;

/**
 * Update form
 */
class UpdateForm extends Model
{
    public $id;
    public $username;
    public $password;
    public $mobile_number;
    public $avatar;
    public $image = false;
    private $_user = false;

    /**
     * UpdateForm constructor.
     * @param array $user
     */
    public function __construct($user)
    {
        $this->_user = $user;
        $this->id = $user->id;
        $this->username = $user->username;
        $this->mobile_number = $user->mobile_number;
        $this->avatar = $user->avatar;

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['mobile_number', 'filter', 'filter' => 'trim'],
            ['mobile_number', 'match',
                'pattern' => '/^([\+][0-9]{1,3}[ \.\-])?([\(]{1}[0-9]{1,6}[\)])?([0-9 \.\-\/]{3,20})((x|ext|extension)[ ]?[0-9]{1,4})?$/',
                'message' => Yii::t('app', 'Your mobile number should be valid german mobile number like +999 999999 or 99999999999.')
            ],

            [['image'], 'safe'],
            [['image'], 'file', 'extensions' => 'jpg, gif, png'],

            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * upload image file
     * @return bool
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\Exception
     */
    public function upload()
    {
        if ($this->image && $this->validate()) {
            $fileName = Yii::$app->security->generateRandomString() . '.' . $this->image->extension;

            $this->image->saveAs(BaseYii::getAlias("@webroot") . '/images/avatar/' . $fileName);
            $this->avatar = Url::to('@web/images/avatar/', true) . $fileName;
            $this->image = null;
        }

        return false;
    }

    /**
     * Update.
     * @return array|bool|null {null | User} whether the user is updated successfully
     * @throws \yii\base\InvalidParamException
     */
    public function update()
    {
        if ($this->validate()) {
            $user = $this->_user;
            $isChanged = false;

            //only updated field will be changed
            $fields = ['username', 'mobile_number', 'avatar'];
            foreach ($fields as $field) {
                if ($this->{$field} !== $user->{$field} && !empty($this->{$field})) {
                    $user->{$field} = $this->{$field};
                    $isChanged = true;
                }
            }

            if (!empty($this->password)) {
                $user->setPassword($this->password);
                $user->generateAuthKey();
                $isChanged = true;
            }

            if ($isChanged) {
                if ($user->save()) {
                    return $user;
                }
                return null;
            }

            return $user;
        }

        return null;
    }
}

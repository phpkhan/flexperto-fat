<?php
use tests\codeception\frontend\AcceptanceTester;
use tests\codeception\common\_pages\LoginPage;

$I = new AcceptanceTester($scenario);
$I->wantTo('ensure login page works');

$loginPage = LoginPage::openBy($I);

$I->amGoingTo('try to login with correct credentials');
$loginPage->login('erau', 'password_0');
$I->expectTo('see that user is logged');
$I->seeLink('Logout (erau)');
$I->dontSeeLink('Login');
$I->dontSeeLink('Signup');
$I->see('Profile');

$I->amGoingTo('try to check profile page');
$I->click('Profile');
$I->see('erau\'s profile');
$I->amGoingTo('try to check edit profile page');
$I->see('edit');
$I->see('delete');
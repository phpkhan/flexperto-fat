<?php
/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<?php $this->registerJsFile(Url::to("@web/js/angular.min.js")) ?>
<?php $this->registerJsFile(Url::to("//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-route.js")) ?>
<?php $this->registerJsFile(Url::to("@web/js/app.js")) ?>
<?php $this->registerJsFile(Url::to("@web/js/controllers.js")) ?>
<div class="site-index" ng-app="userListApp">
    <div class="body-content" ng-controller="UserListController">
        <div class="row">
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="ng-view"></div>
                </div>
            </div>
        </div>
    </div>
</div>

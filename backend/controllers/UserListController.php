<?php
namespace backend\controllers;

use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\User;

/**
 * Site controller
 */
class UserListController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['all'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['user'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string
     */
    public function actionAll()
    {
        $users = User::find()->all();

        $usersData = [];
        $removeKeys = ['auth_key', 'password_hash', 'password_reset_token'];
        foreach ($users as $key => $user) {
            $usersData[] = array_diff_key(json_decode(\yii\helpers\Json::encode($user), true), array_flip($removeKeys));
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $usersData;
    }

    /**
     * @return string
     */
    public function actionUser()
    {
        $user = User::findOne(['id' => Yii::$app->request->get('user-id')]);

        $removeKeys = ['auth_key', 'password_hash', 'password_reset_token'];
        $user = array_diff_key(json_decode(\yii\helpers\Json::encode($user), true), array_flip($removeKeys));

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $user;
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\Exception
     */
    public function actionEdit()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Url::to('login'));
        }

        $user = User::findOne(['id' => Yii::$app->request->get('user-id')]);
        if ($data = Yii::$app->request->get()) {
            if (array_key_exists('username', $data) && $data['username'] !== '') {
                $user->username = $data['username'];
            }

            if (array_key_exists('email', $data) && $data['email'] !== '') {
                $user->email = $data['email'];
            }

            if (array_key_exists('mobile_number', $data) && $data['mobile_number'] !== '') {
                $user->mobile_number = $data['mobile_number'];
            }
            
            if (array_key_exists('password', $data) && $data['password'] !== '') {
                $user->setPassword($data['password']);
            }

            $user->save();
            $response = ['status' => 'success', 'message' => 'Account updated successfully.'];
        } else {
            $response = ['status' => 'error', 'message' => 'failed to update account.'];
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $response;
    }

    /**
     * @return array
     * @throws \yii\web\NotAcceptableHttpException
     */
    public function actionDelete()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Url::to('login'));
        }

        $user = $user = User::findOne(['id' => Yii::$app->request->get('user-id')]);
        $user->status = User::STATUS_DELETED;

        if ($user->save()) {
            $response = ['status' => 'success', 'message' => 'Account deleted successfully.'];
        } else {
            $response = ['status' => 'error', 'message' => 'failed to delete account.'];
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $response;
    }
}

userListApp.controller("UserListController", ['$scope', '$http', function ($scope, $http) {
    $scope.APIUrl = '/index.php?r=user-list%2Fall';
    $http.get($scope.APIUrl).success(function (json, status, headers, config) {
        $scope.users = json;
    }).error(function (data, status, headers, config) {
        // log error
        console.log('AJAX FAILED TO LOAD');
    });

    $scope.delete = function ($id) {
        var r = confirm("Do you want to delete the account ?!");
        if (r == true) {
            $scope.DeleteAPIUrl = '/index.php?r=user-list%2Fdelete&user-id=' + $id;
            $http.get($scope.DeleteAPIUrl).success(function (json, status, headers, config) {
                $http.get($scope.APIUrl).success(function (json, status, headers, config) {
                    $scope.users = json;
                }).error(function (data, status, headers, config) {
                    // log error
                    console.log('AJAX FAILED TO LOAD');
                });
                alert(json.message);
            }).error(function (data, status, headers, config) {
                console.log('AJAX FAILED TO LOAD');
            });
        }
    }
}]);

userListApp.controller("UserEditController", ['$scope', '$http', '$routeParams', '$httpParamSerializerJQLike', function ($scope, $http, $routeParams, $httpParamSerializerJQLike) {
    $scope.UserId = $routeParams.id;
    $scope.APIUrl = '/index.php?r=user-list%2Fuser&user-id=' + $scope.UserId;
    $http.get($scope.APIUrl).success(function (json, status, headers, config) {
        $scope.user = json;
    }).error(function (data, status, headers, config) {
        // log error
        console.log('AJAX FAILED TO LOAD');
    });

    $scope.update = function () {
        $http.get(
            '/index.php?r=user-list%2Fedit&user-id=' + $scope.UserId + $httpParamSerializerJQLike($scope.user)
        ).success(function (json, status, headers, config) {
            alert(json.message);
            $scope.APIUrl = '/index.php?r=user-list%2Fuser&user-id=' + $scope.UserId;
            $http.get($scope.APIUrl).success(function (json, status, headers, config) {
                $scope.user = json;
            }).error(function (data, status, headers, config) {
                // log error
                console.log('AJAX FAILED TO LOAD');
            });
        }).error(function (data, status, headers, config) {
            // log error
            console.log('AJAX FAILED TO LOAD');
        });
    }
}]);
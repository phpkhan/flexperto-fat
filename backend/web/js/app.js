var userListApp = angular.module('userListApp', ['ngRoute']);


userListApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
    when('/', {
        redirectTo: '/list'
    }).
    when('/list', {
        templateUrl: '/partials/list.html',
        controller: 'UserListController'
    }).
    when('/edit/:id', {
        templateUrl: '/partials/edit.html',
        controller: 'UserEditController'
    }).
    otherwise({
        redirectTo: '/not-available'
    });
}]);